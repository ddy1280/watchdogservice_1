﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Diagnostics;
using System.ServiceProcess;

namespace WdgService
{
    public partial class SettingDlg : Form
    {
        private Desktop m_Desktop = new Desktop();
        private IContainer m_Container = null;
        private NotifyIcon m_NotifyIcon = null;
        private Button btnHide;
        private ContextMenu m_ContextMenu = null;

        /// <summary>
        /// Start the UI thread
        /// </summary>
        public static SettingDlg StartUIThread()
        {
            SettingDlg dlg = new SettingDlg();

            Thread thread = new Thread(new ThreadStart(dlg.UIThread));
            thread.Start();

            return dlg;
        }

        /// <summary>
        /// UI thread
        /// </summary>
        public void UIThread()
        {
            if( !m_Desktop.BeginInteraction() )
                return;
            Application.Run(this);
        }

        protected SettingDlg()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Move the window to the right-bottom corner
        /// </summary>
        /// <param name="e"></param>
        protected override void OnShown(EventArgs e)
        {
            this.Left = Screen.PrimaryScreen.WorkingArea.Left
                + Screen.PrimaryScreen.WorkingArea.Width
                - this.Width
                ;
            this.Top = Screen.PrimaryScreen.WorkingArea.Top
                + Screen.PrimaryScreen.WorkingArea.Height
                - this.Height
                ;
        }


        private void SettingDlg_Load(object sender, EventArgs e)
        {
            m_ContextMenu = new ContextMenu();
            MenuItem mItem;
            mItem = m_ContextMenu.MenuItems.Add("Open Dialog", this.OpenDialog);
            mItem.Name = "OpenDialog";
            mItem = m_ContextMenu.MenuItems.Add("Start Watchdog", this.StartWatchdog);
            mItem.Name = "StartWatchdog";
            mItem = m_ContextMenu.MenuItems.Add("Stop Watchdog", this.StopWatchdog);
            mItem.Name = "StopWatchdog";
                             
            Icon icon = new Icon(SystemIcons.Application, 16, 16);
            m_Container = new Container();
            m_NotifyIcon = new NotifyIcon(m_Container);
            m_NotifyIcon.ContextMenu = m_ContextMenu;
            m_NotifyIcon.Icon = icon;
            m_NotifyIcon.Visible = true;

            m_NotifyIcon.ShowBalloonTip( 200
                , "SystemTrayIconInSvc"
                , "The system tray icon is implemented in the windows service itself."
                , ToolTipIcon.Info
                );

            if ((ServiceManager.GetServiceStatus() == System.ServiceProcess.ServiceControllerStatus.Running)
                || (ServiceManager.GetServiceStatus() == System.ServiceProcess.ServiceControllerStatus.ContinuePending)
                || (ServiceManager.GetServiceStatus() == System.ServiceProcess.ServiceControllerStatus.StartPending))
            
            {
                m_ContextMenu.MenuItems["StartWatchdog"].Enabled = false;
                m_ContextMenu.MenuItems["StopWatchdog"].Enabled = true;
            }
            else
            {
                m_ContextMenu.MenuItems["StartWatchdog"].Enabled = true;
                m_ContextMenu.MenuItems["StopWatchdog"].Enabled = false;
            }
             
        }



        public void OpenDialog(Object sender, EventArgs e)
        {
            this.Visible = true;
            BringToFront();
        }

        public void StartWatchdog(Object sender, EventArgs e)
        {
            ServiceManager.Start();
            if ((ServiceManager.GetServiceStatus() == System.ServiceProcess.ServiceControllerStatus.Running)
                || (ServiceManager.GetServiceStatus() == System.ServiceProcess.ServiceControllerStatus.ContinuePending)
                || (ServiceManager.GetServiceStatus() == System.ServiceProcess.ServiceControllerStatus.StartPending))
            
            {
                m_ContextMenu.MenuItems["StartWatchdog"].Enabled = false;
                m_ContextMenu.MenuItems["StopWatchdog"].Enabled = true;
            }

        }

        public void StopWatchdog(Object sender, EventArgs e)
        {
            ServiceManager.Stop();
            if ((ServiceManager.GetServiceStatus() == System.ServiceProcess.ServiceControllerStatus.Stopped)
                || (ServiceManager.GetServiceStatus() == System.ServiceProcess.ServiceControllerStatus.PausePending)
                || (ServiceManager.GetServiceStatus() == System.ServiceProcess.ServiceControllerStatus.StopPending))
            {
                m_ContextMenu.MenuItems["StartWatchdog"].Enabled = true;
                m_ContextMenu.MenuItems["StopWatchdog"].Enabled = false;
            }
        }

        protected override void OnClosed(EventArgs e)
        {
            m_NotifyIcon.Dispose();
            m_ContextMenu.Dispose();
            m_Container.Dispose();
        }

        private void btnHide_Click(object sender, EventArgs e)
        {
            this.Visible = false;
        }

   

    }
}
