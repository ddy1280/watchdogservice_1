﻿using System.Collections.Generic;
using System.ServiceProcess;
using System.Text;
using System.Management;
using System.Runtime.InteropServices;
using System;
using System.Diagnostics;
using System.Windows.Forms;
using System.Threading;
using System.Drawing;


namespace WdgService
{
    public class Executor
    {
        static public string RunDosCommand(string command)
        {
            using (Process proc = new Process())
            {
                string retString = "";
                proc.StartInfo.FileName = "cmd.exe";
                proc.StartInfo.UseShellExecute = false;
                proc.StartInfo.RedirectStandardInput = true;
                proc.StartInfo.RedirectStandardOutput = true;
                proc.StartInfo.CreateNoWindow = true;
                proc.Start();
                proc.StandardInput.AutoFlush = true;
                proc.StandardInput.WriteLine(command);
                proc.StandardInput.WriteLine("exit");
                proc.WaitForExit();
                retString = proc.StandardOutput.ReadToEnd();
                proc.Close();
                return retString;
            }
        }


        static public int RunProgram(string progFilename, string param)
        {
            int Pid = 0;

            System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
            //設置運行文件 

            startInfo.WorkingDirectory = System.IO.Path.GetDirectoryName(progFilename);
            startInfo.UseShellExecute = false;
            startInfo.FileName = progFilename;
            //設置啟動參數 
            startInfo.Arguments = param;

            Process proc = new Process();
            proc = Process.Start(startInfo);
            Pid = proc.Id;

            //   proc.Dispose();
            return Pid;
        }
    }

    static public class ServiceManager
    {

        public static string serviceName = System.AppDomain.CurrentDomain.FriendlyName;
        static ServiceManager()
        {
            serviceName = System.IO.Path.GetFileNameWithoutExtension(serviceName);
        }


        /// <summary>
        ///  指定 Windows Service 服務操作
        /// </summary>
        static private bool StartService(string servName, bool status)
        {
            bool ret = false;
            ServiceController SC;
            SC = new ServiceController(servName);
            if (status == false)
            {
                if (SC.CanStop)
                {
                    SC.Stop();
                    ret = true;
                }
            }
            else if (status == true)
            {
                if ((SC.Status.ToString().ToUpper() != "STARTED") & (SC.Status.ToString().ToUpper() != "PENDING START"))
                {
                    SC.Start();
                    ret = true;
                }
            }
            SC.Dispose();
            return ret;
        }

        static public void Install()
        {
            string filename = System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName;
            string exefile = @"%SystemRoot%\Microsoft.NET\Framework\v2.0.50727\InstallUtil";
            string exeparam = String.Format("/name=S1 {0}", filename);

            string ret = Executor.RunDosCommand(exefile + " " + exeparam);
            Console.WriteLine(ret);
        }
        static public void Uninstall()
        {
            string ret = Executor.RunDosCommand("SC delete " + serviceName);
            Console.WriteLine(ret);
        }
        static public bool Start()
        {
            return StartService(serviceName, true);
            //string ret = Executor.RunDosCommand("NET START " + serviceName);
            //Console.WriteLine(ret);
        }
        static public bool Stop()
        {
            return StartService(serviceName, false);
            //string ret = Executor.RunDosCommand("NET STOP " + serviceName);
            //Console.WriteLine(ret);
        }
        static public System.ServiceProcess.ServiceControllerStatus GetServiceStatus()
        {
            try
            {
                using (ServiceController SC = new ServiceController(serviceName))
                {
                    return SC.Status;
                }
            }
            catch 
            {
                
                return System.ServiceProcess.ServiceControllerStatus.Stopped;
            }

        }


    }

    static class Program
    {

        /// <summary>
        /// 應用程式的主要進入點。
        /// </summary>
        static void Main(string[] args)
        {
            if (args.Length > 0)
            {
                if ( args[0].ToUpper() == "/INSTALL")
                {
                    ServiceManager.Install();
                }
                else if (args[0].ToUpper() == "/UNINSTALL")
                {
                    ServiceManager.Uninstall();
                }
                else if (args[0].ToUpper() == "/START")
                {
                    ServiceManager.Start();

                }
                else if (args[0].ToUpper() == "/STOP")
                {
                    ServiceManager.Stop();
                }
                else if (args[0].ToUpper() == "/DEBUG")
                {//以一般程式執行

                    LauncherService testService = new LauncherService();
                    testService.StartService();
                    testService.SetDebug(true);
                    System.Threading.Thread.Sleep(System.Threading.Timeout.Infinite);
                }
                else if (args[0].ToUpper() == "/UI")
                {
                    //SettingDlg.StartUIThread();
                    SystemTrayCreator.Create();

                }
                else
                {
                    Console.WriteLine("Command Error!");
                }

            }
            else
            {

                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[] { new LauncherService() };
                ServiceBase.Run(ServicesToRun);
            }


        }

    }


}