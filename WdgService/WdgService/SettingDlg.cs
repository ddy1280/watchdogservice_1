﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Diagnostics;
using System.ServiceProcess;

namespace WdgService
{
#region SettingDlg 這段code 暫時保留不用
    public partial class SettingDlg : Form
    {
        private Desktop m_Desktop = new Desktop();
        private IContainer m_Container = null;
        private NotifyIcon m_NotifyIcon = null;
        private ContextMenu m_ContextMenu = null;

        /// <summary>
        /// Start the UI thread
        /// </summary>
        public static SettingDlg StartUIThread()
        {
            //檢查Mutex, 只允許一個Instance

            SettingDlg dlg = new SettingDlg();

            Thread thread = new Thread(new ThreadStart(dlg.UIThread));
            thread.Start();

            return dlg;
        }

        /// <summary>
        /// UI thread
        /// </summary>
        public void UIThread()
        {
            if( !m_Desktop.BeginInteraction() )
                return;

            bool createNew;

            Mutex mutex = new Mutex(true, "WDGSERVICE_MUTEX", out createNew);
            if (!createNew)
            {
                return;
            }
            
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(this);
            mutex.ReleaseMutex();

        }

        protected SettingDlg()
        {
            InitializeComponent();

            lvWD.Columns.Add(new ColumnHeader());
            lvWD.Columns.Add(new ColumnHeader());
            lvWD.Columns[0].Text = "Process Name";
            lvWD.Columns[1].Text = "Process Location";
            


            ReloadWatchdogList();

        }

        /// <summary>
        /// Move the window to the right-bottom corner
        /// </summary>
        /// <param name="e"></param>
        protected override void OnShown(EventArgs e)
        {
            this.Left = Screen.PrimaryScreen.WorkingArea.Left
                + Screen.PrimaryScreen.WorkingArea.Width
                - this.Width - 5
                ;
            this.Top = Screen.PrimaryScreen.WorkingArea.Top
                + Screen.PrimaryScreen.WorkingArea.Height
                - this.Height
                ;
        }


        private void SettingDlg_Load(object sender, EventArgs e)
        {
            m_ContextMenu = new ContextMenu();
            MenuItem mItem;
            mItem = m_ContextMenu.MenuItems.Add("Open Watchdog List", this.OpenDialog);
            mItem.Name = "OpenDialog";
            mItem = m_ContextMenu.MenuItems.Add("Start Watchdog", this.StartWatchdog);
            mItem.Name = "StartWatchdog";
            mItem = m_ContextMenu.MenuItems.Add("Stop Watchdog", this.StopWatchdog);
            mItem.Name = "StopWatchdog";
            mItem = m_ContextMenu.MenuItems.Add("Exit", this.ExitUI);
            mItem.Name = "Exit";
                             
            Icon icon = new Icon(SystemIcons.Application, 16, 16);
            m_Container = new Container();
            m_NotifyIcon = new NotifyIcon(m_Container);
            m_NotifyIcon.ContextMenu = m_ContextMenu;
            m_NotifyIcon.Icon = icon;
            m_NotifyIcon.Visible = true;

            m_NotifyIcon.ShowBalloonTip(200
                , "WdgService"
                , "WdgService is running."
                , ToolTipIcon.Info
                );

            this.Visible = false;

            if ((ServiceManager.GetServiceStatus() == System.ServiceProcess.ServiceControllerStatus.Running)
                || (ServiceManager.GetServiceStatus() == System.ServiceProcess.ServiceControllerStatus.ContinuePending)
                || (ServiceManager.GetServiceStatus() == System.ServiceProcess.ServiceControllerStatus.StartPending))
            
            {
                m_ContextMenu.MenuItems["StartWatchdog"].Enabled = false;
                m_ContextMenu.MenuItems["StopWatchdog"].Enabled = true;
            }
            else
            {
                m_ContextMenu.MenuItems["StartWatchdog"].Enabled = true;
                m_ContextMenu.MenuItems["StopWatchdog"].Enabled = false;
            }
             
        }



        public void OpenDialog(Object sender, EventArgs e)
        {

            this.Visible = true;
            ReloadWatchdogList();
            BringToFront();
        }

        public void StartWatchdog(Object sender, EventArgs e)
        {
            ServiceManager.Start();
            if ((ServiceManager.GetServiceStatus() == System.ServiceProcess.ServiceControllerStatus.Running)
                || (ServiceManager.GetServiceStatus() == System.ServiceProcess.ServiceControllerStatus.ContinuePending)
                || (ServiceManager.GetServiceStatus() == System.ServiceProcess.ServiceControllerStatus.StartPending))
            
            {
                m_ContextMenu.MenuItems["StartWatchdog"].Enabled = false;
                m_ContextMenu.MenuItems["StopWatchdog"].Enabled = true;
            }

        }

        public void StopWatchdog(Object sender, EventArgs e)
        {
            ServiceManager.Stop();
            if ((ServiceManager.GetServiceStatus() == System.ServiceProcess.ServiceControllerStatus.Stopped)
                || (ServiceManager.GetServiceStatus() == System.ServiceProcess.ServiceControllerStatus.PausePending)
                || (ServiceManager.GetServiceStatus() == System.ServiceProcess.ServiceControllerStatus.StopPending))
            {
                m_ContextMenu.MenuItems["StartWatchdog"].Enabled = true;
                m_ContextMenu.MenuItems["StopWatchdog"].Enabled = false;
            }
        }

        public void ExitUI(Object sender, EventArgs e)
        {

            Application.ExitThread();
        }

        protected override void OnClosed(EventArgs e)
        {
            m_NotifyIcon.Dispose();
            m_ContextMenu.Dispose();
            m_Container.Dispose();
        }

        private void ReloadWatchdogList()
        {
            lvWD.Items.Clear();

            Microsoft.Win32.RegistryKey start = Microsoft.Win32.Registry.LocalMachine;
            Microsoft.Win32.RegistryKey wdgRegList = start.OpenSubKey("Software\\WdgReg\\");

            foreach (string progName in wdgRegList.GetValueNames())
            {
                ListViewItem item = new ListViewItem();
                item.Text = progName;
                item.Name = progName;
                item.ImageKey = progName;
                item.SubItems.Add(wdgRegList.GetValue(progName).ToString());
                lvWD.Columns[0].Width = -2;  //讓column 依項目內容長度自動調整
                lvWD.Items.Add(item);
            }
            wdgRegList.Close();
            start.Close();

        }

        private void SettingDlg_FormClosing(object sender, FormClosingEventArgs e)
        {

            this.Hide();
            e.Cancel = true;
        }
    }
#endregion
    public class SystemTrayCreator
    {
        private NotifyIcon m_NotifyIcon = null;
        private ContextMenu m_ContextMenu = null;

        /// <summary>
        /// Start the UI thread
        /// </summary>
        public static SystemTrayCreator Create()
        {
            //檢查Mutex, 只允許一個Instance

            SystemTrayCreator dlg = new SystemTrayCreator();

            Thread thread = new Thread(new ThreadStart(dlg.UIThread));
            thread.Start();

            return dlg;
        }

        private void UIThread()
        {

            bool createNew;
            Mutex mutex = new Mutex(true, "WDGSERVICE_MUTEX", out createNew);
            if (!createNew)
            {
                return;
            }
            
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);


            m_ContextMenu = new ContextMenu();
            MenuItem mItem;
            mItem = m_ContextMenu.MenuItems.Add("Start Watchdog", StartWatchdog);
            mItem.Name = "StartWatchdog";
            mItem = m_ContextMenu.MenuItems.Add("Stop Watchdog", StopWatchdog);
            mItem.Name = "StopWatchdog";
            mItem = m_ContextMenu.MenuItems.Add("Exit", ExitUI);
            mItem.Name = "Exit";

            Icon icon = new Icon(SystemIcons.Application, 16, 16);

            m_NotifyIcon = new NotifyIcon();
            m_NotifyIcon.ContextMenu = m_ContextMenu;
            m_NotifyIcon.Icon = icon;
            m_NotifyIcon.Visible = true;
            m_NotifyIcon.Text = "Watchdog";

            m_NotifyIcon.ShowBalloonTip(200
                , "WdgService"
                , "WdgService is running."
                , ToolTipIcon.Info
                );


            if ((ServiceManager.GetServiceStatus() == System.ServiceProcess.ServiceControllerStatus.Running)
                || (ServiceManager.GetServiceStatus() == System.ServiceProcess.ServiceControllerStatus.ContinuePending)
                || (ServiceManager.GetServiceStatus() == System.ServiceProcess.ServiceControllerStatus.StartPending))
            {
                m_ContextMenu.MenuItems["StartWatchdog"].Enabled = false;
                m_ContextMenu.MenuItems["StopWatchdog"].Enabled = true;
            }
            else
            {
                m_ContextMenu.MenuItems["StartWatchdog"].Enabled = true;
                m_ContextMenu.MenuItems["StopWatchdog"].Enabled = false;
            }
            Application.Run();
            mutex.ReleaseMutex();

        }


        private void StartWatchdog(Object sender, EventArgs e)
        {
            ServiceManager.Start();
            if ((ServiceManager.GetServiceStatus() == System.ServiceProcess.ServiceControllerStatus.Running)
                || (ServiceManager.GetServiceStatus() == System.ServiceProcess.ServiceControllerStatus.ContinuePending)
                || (ServiceManager.GetServiceStatus() == System.ServiceProcess.ServiceControllerStatus.StartPending))
            
            {
                m_ContextMenu.MenuItems["StartWatchdog"].Enabled = false;
                m_ContextMenu.MenuItems["StopWatchdog"].Enabled = true;

            }

        }

        private void StopWatchdog(Object sender, EventArgs e)
        {
            ServiceManager.Stop();
            if ((ServiceManager.GetServiceStatus() == System.ServiceProcess.ServiceControllerStatus.Stopped)
                || (ServiceManager.GetServiceStatus() == System.ServiceProcess.ServiceControllerStatus.PausePending)
                || (ServiceManager.GetServiceStatus() == System.ServiceProcess.ServiceControllerStatus.StopPending))
            {
                m_ContextMenu.MenuItems["StartWatchdog"].Enabled = true;
                m_ContextMenu.MenuItems["StopWatchdog"].Enabled = false;
            }
        }

        private void ExitUI(Object sender, EventArgs e)
        {
            Application.ExitThread();
        }

    }
    //===================================================================================================
}
