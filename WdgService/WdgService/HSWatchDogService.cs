﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;
using System.Management;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace WdgService
{
    public partial class LauncherService : ServiceBase
    {
        private Thread MonitoringThread;
        private bool IsUIStarted=false;
        private bool SetDebugFlag = false;
        private class ProgramInfo_S 
        {
            public int RestartCount = 0;
            public string ProgFileName;
            public ProgramInfo_S(string progfilename, bool MonitoringState)
            {
                ProgFileName = progfilename;
            }


        }

        private Dictionary<string, ProgramInfo_S> progList;
        public LauncherService()
        {
            InitializeComponent();
            progList = new Dictionary<string, ProgramInfo_S>();

        }

        protected override void OnStart(string[] args)
        {
            StartService();

        }

        protected override void OnStop()
        {
            StopService();
        }

        public void StartService()
        {
            MonitoringThread = new Thread(WatchDogHandler);
            MonitoringThread.Start();


        }



        public void StopService()
        {
            if (MonitoringThread.IsAlive)
            {
                MonitoringThread.Abort();
                IsUIStarted = false;
            }
        }



        private string GetProcessParameter(string progName)
        {
            ManagementClass mgmtClass = new ManagementClass("Win32_Process");
            foreach (ManagementObject procobj in mgmtClass.GetInstances())
            {
                string procname = procobj["Name"].ToString();

                string name = progName + ".EXE";
                if (procname.ToUpper() == name)
                {

                    if (procobj["CommandLine"] != null)
                    {
                        string cmd = procobj["CommandLine"].ToString();
                        int dotExePos = cmd.IndexOf(".EXE");
                        cmd = cmd.Substring(cmd.IndexOf(" ", dotExePos)+1);
                        return cmd;
                    }
                }
            }
            return "";
        }
 
        private void WatchDogHandler()
        {
            UInt32 cnt = 0;

            while (true)
            {

                if (IsUIStarted == false)
                {
                    IntPtr m_hCurDesktop = User32DLL.GetDesktopWindow();
                    if (m_hCurDesktop != IntPtr.Zero)
                    {
                        IsUIStarted = CreateServiceUI(); //if success, return TRUE
                    }
                }
     

                ReadMonitoringPrograms();
                     
                foreach (string progName in progList.Keys)
                {
                                 
                    Process[] progs = Process.GetProcessesByName(progName);

                    if (progs.Length == 0)
                    {
                        string progFilename = progList[progName].ProgFileName;
                        string param = GetProcessParameter(progName);
                        progList[progName].RestartCount++;
                        //RunProgram(progFilename, param);
                        WdgService.ServiceExecutor.PROCESS_INFORMATION procInfo = new WdgService.ServiceExecutor.PROCESS_INFORMATION();
                        ServiceExecutor.StartProcessAndBypassUAC(progFilename, param, out procInfo);
                        StringBuilder sb = new StringBuilder();
                        sb.AppendFormat("NotFound: Restart {0}, Count:{1}\n", progName, progList[progName].RestartCount);
                        LogMessage(sb.ToString(), EventLogEntryType.Information);
                    }
                    else
                    {
                        foreach (Process proc in progs)
                        {
                            if (!proc.HasExited)
                            {
                                // Refresh the current process property values.
                                proc.Refresh();

                                if ((cnt % 30) == 0)
                                {//每30秒記錄一次目前程式運行資訊
                                    StringBuilder sb = new StringBuilder();
                                    sb.AppendFormat("PID:{0}-{1}\n", proc.Id, progName);
                                    sb.AppendFormat("Physical memory usage:{0} KB\n", proc.WorkingSet64 / 1024);
                                    sb.AppendFormat("Virtual memory usage:{0} KB\n", proc.VirtualMemorySize64 / 1024);
                                    LogMessage(sb.ToString(), EventLogEntryType.Information);
                                    cnt = 0;
                                }

                                if (!proc.Responding)
                                {
                                    Executor.RunDosCommand("taskkill /F /PID " + proc.Id.ToString());

                                    LogMessage(proc.Id.ToString() + ":" + progName + " no response!", EventLogEntryType.Error);
                                    //proc.Kill();

                                    progList[progName].RestartCount++;
                                    string progFilename = proc.MainModule.FileName;
                                    string param = GetProcessParameter(progName);
                                    //int pid = RunProgram(progFilename, param);
                                    WdgService.ServiceExecutor.PROCESS_INFORMATION procInfo = new WdgService.ServiceExecutor.PROCESS_INFORMATION();
                                    ServiceExecutor.StartProcessAndBypassUAC(progFilename, param,out procInfo);
                                    UInt32 pid = procInfo.dwProcessId;

                                    StringBuilder sb = new StringBuilder();
                                    sb.AppendFormat("Restart {0}, Count:{1}\n", progName,progList[progName].RestartCount);
                                    LogMessage(sb.ToString(), EventLogEntryType.Information);

                                    Executor.RunDosCommand("taskkill /F /IM dw20.exe");

                                }

                                proc.Dispose();
                            }
                        }


                    }

                }
                cnt++;
                Thread.Sleep(1000);
            }

        }


        //讀取有多少程式要加入watchdog檢查
        private void ReadMonitoringPrograms()
        {//從Registry 讀取欲watchdog 的程式資訊

            Microsoft.Win32.RegistryKey start = Microsoft.Win32.Registry.LocalMachine;
            Microsoft.Win32.RegistryKey wdgRegList = start.OpenSubKey("Software\\WdgReg\\");
             
            progList.Clear();
            foreach (string progName in wdgRegList.GetValueNames())
            {
                progList.Add(progName, new ProgramInfo_S(wdgRegList.GetValue(progName).ToString(), true));
            }
            wdgRegList.Close();
            start.Close();
        }


        private void LogMessage(string message, EventLogEntryType type)
        {
            EventLog.WriteEntry(ServiceName, message, type);

            Console.WriteLine(ServiceName +" " + type.ToString());
            Console.WriteLine(message);
            Console.WriteLine("-----------------------------------------------------------\n");
        }
        public void SetDebug(bool debug)
        {
            SetDebugFlag = debug;
        }
        private bool CreateServiceUI()
        {
            string serviceSelf  = System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName;

            if (SetDebugFlag)
            {
                SettingDlg.StartUIThread();
               // Executor.RunProgram(serviceSelf, "/UI");
                return true;
            }
            else
            {
                WdgService.ServiceExecutor.PROCESS_INFORMATION procInfo = new WdgService.ServiceExecutor.PROCESS_INFORMATION();
                return ServiceExecutor.StartProcessAndBypassUAC(serviceSelf, "/UI", out procInfo);
            }
        }
    }
}
