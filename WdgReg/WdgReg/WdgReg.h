#ifndef WDGREG_H
#define WDGREG_H
#include "Windows.h"

#ifdef WDGREG_H_EXPORT
#define DllExport __declspec(dllexport)
#else
#define DllExport __declspec(dllimport)
#endif

extern "C"
{
	DllExport void WatctdogInit(void);
	DllExport void WatctdogCancel(void);

};

/* 
 * C# Interface
#region DLL�ŧi

[DllImport("WdgReg.dll", CallingConvention = CallingConvention.Cdecl)]
private static extern void WatctdogInit();

[DllImport("WdgReg.dll", CallingConvention = CallingConvention.Cdecl)]
private static extern void WatctdogCancel();


#endregion
 */

#endif
