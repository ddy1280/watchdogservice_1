// dllmain.cpp : 定義 DLL 應用程式的進入點。

#include "stdafx.h"
#include "Windows.h"
#include "WdgReg.h"
#include "NTProcessInfo.h"

#include <iostream>
using namespace std;


HKEY OpenKey(HKEY hRootKey, wchar_t* strKey);
std::wstring GetCurrentProcessName();
std::wstring GetCurrentProcessFileName();
void RegisterWatctdog();
void UnregisterWatctdog();

#pragma region DLL進入點
BOOL APIENTRY DllMain( HMODULE hModule,
					  DWORD  ul_reason_for_call,
					  LPVOID lpReserved
					  )
{

	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
		{
			RegisterWatctdog();
			break;
		}
	case DLL_THREAD_ATTACH:	 break;
	case DLL_THREAD_DETACH:	  break;
	case DLL_PROCESS_DETACH:
		{
			UnregisterWatctdog();
			break;
		}
		
	}
	return TRUE;
}
#pragma endregion

DllExport void WatctdogInit(void)
{


}
DllExport void WatctdogCancel(void)
{
	UnregisterWatctdog();
}

///====================================================================================================
void RegisterWatctdog()
{

	std::wstring filename = GetCurrentProcessFileName();
    std::wstring progname = GetCurrentProcessName();

	HKEY key = OpenKey(HKEY_LOCAL_MACHINE,L"Software\\WdgReg\\");
	if (RegSetValueEx(key, progname.c_str(), 0, REG_SZ, (LPBYTE)filename.c_str(), filename.size()*sizeof(wchar_t)) == ERROR_SUCCESS)
	{
		wcout << L"WdgReg:" << progname.c_str() << L"is monitoring!" << endl;
	}
	RegCloseKey(key);

}

void UnregisterWatctdog()
{
	std::wstring filename = GetCurrentProcessFileName();
	std::wstring progname = GetCurrentProcessName();

	HKEY key;
	if (RegOpenKey(HKEY_LOCAL_MACHINE, TEXT("Software\\WdgReg\\"), &key) == ERROR_SUCCESS)
	{
		wcout << L"WdgReg:" <<progname.c_str() << L"is not monitoring!" << endl;
	}



	RegDeleteValue(key, progname.c_str());
	RegCloseKey(key);
}


HKEY OpenKey(HKEY hRootKey, wchar_t* strKey)
{
	HKEY hKey;
	LONG nError = RegOpenKeyEx(hRootKey, strKey, NULL, KEY_ALL_ACCESS, &hKey);

	if (nError==ERROR_FILE_NOT_FOUND)
	{
		cout << "Creating registry key: " << strKey << endl;
		nError = RegCreateKeyEx(hRootKey, strKey, NULL, NULL, REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL, &hKey, NULL);
	}

	if (nError)
		cout << "Error: " << nError << " Could not find or create " << strKey << endl;

	return hKey;
}


std::wstring GetCurrentProcessFileName()
{
	HMODULE hNtDll = sm_LoadNTDLLFunctions();
	if(hNtDll == NULL)
	{
		printf("Failed to load NTDLL functions");
		return L"";
	}

	DWORD pid = GetCurrentProcessId();
	smPROCESSINFO pi;
	sm_GetNtProcessInfo(pid,&pi);

	if (hNtDll != NULL) sm_FreeNTDLLFunctions(hNtDll);


	return std::wstring(pi.szImgPath);

}

std::wstring GetCurrentProcessName()
{
	std::wstring fname = GetCurrentProcessFileName();

	int start = fname.rfind(L"\\")+1   ;
	int end =  fname.rfind(L".")   ; 
	fname = fname.substr(start,end-start);
	return fname;

}

